import { NgModule } from '@angular/core';
import {BsDropdownDirective, BsDropdownModule, CollapseModule} from 'ngx-bootstrap';
import {NgbCollapseModule, NgbModule} from '@ng-bootstrap/ng-bootstrap';

@NgModule({
  declarations: [],
  imports: [
    CollapseModule.forRoot(),
    BsDropdownModule.forRoot(),
    NgbModule,
    NgbCollapseModule
  ],
  exports: [
    CollapseModule,
    BsDropdownModule
  ]
})
export class BootstrapModule { }
