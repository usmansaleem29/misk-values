import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {BootstrapModule} from './bootsrap/bootstrap.module';
import {HeaderComponent} from '../components/header/header.component';
import {MainComponent} from '../components/main/main.component';
import {FooterComponent} from '../components/footer/footer.component';
import {NgbCollapseModule} from '@ng-bootstrap/ng-bootstrap';
import {RightContainerComponent} from '../components/main/right-container/right-container.component';
import {LeftContainerComponent} from '../components/main/left-container/left-container.component';

@NgModule({
  declarations: [HeaderComponent, MainComponent, FooterComponent, RightContainerComponent, LeftContainerComponent],

  imports: [
    CommonModule,
    BootstrapModule,
    NgbCollapseModule
  ],
  exports: [
    BootstrapModule,
    HeaderComponent,
    MainComponent,
    FooterComponent
  ]
})
export class SharedModule {
}
