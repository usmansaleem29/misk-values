import {ChangeDetectionStrategy, Component, Input, OnChanges, OnInit, SimpleChanges} from '@angular/core';
import {animate, keyframes, query, stagger, style, transition, trigger} from '@angular/animations';

@Component({
  selector: 'app-right-container',
  templateUrl: './right-container.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush,
  animations: [
    // Trigger animation cards array
    trigger('listAnimation', [
      // Transition from any state to any state
      transition('* => *', [
        // Initially the all cards are not visible
        query(':enter', style({opacity: 0}), {optional: true}),

        // Each card will appear sequentially with the delay of 300ms
        query(':enter', stagger('50ms', [
          animate('.5s ease-in', keyframes([
            style({opacity: 0, transform: 'translateY(-50%)', offset: 0}),
            style({opacity: 1, transform: 'translateY(0)', offset: 1}),
          ]))]), {optional: true}),
      ]),
    ]),
  ],
  styleUrls: ['./right-container.component.css']
})
export class RightContainerComponent implements OnInit, OnChanges {

  @Input() data: any;

  @Input() key: string;

  culturalValuesImages: any;
  desiredValuesImages: any;

  constructor() {
  }

  ngOnInit() {
  }

  changeCulturalImages() {
    return this.culturalValuesImages = [
      '../../../../assets/images/shape1.svg',
      '../../../../assets/images/shape2.svg',
      '../../../../assets/images/shape3.svg',
      '../../../../assets/images/shape4.svg',
      '../../../../assets/images/shape5.svg'
    ].reduce((acc, el) => {
      Math.random() > 0.5 ? acc.push(el) : acc.unshift(el);
      return acc;
    }, []);
  }

  changeDesiredImages() {
    return this.desiredValuesImages = [
      '../../../../assets/images/shape1.svg',
      '../../../../assets/images/shape2.svg',
      '../../../../assets/images/shape3.svg',
      '../../../../assets/images/shape4.svg',
      '../../../../assets/images/shape5.svg'
    ].reduce((acc, el) => {
      Math.random() > 0.5 ? acc.push(el) : acc.unshift(el);
      return acc;
    }, []);
  }

  ngOnChanges(changes: SimpleChanges): void {
    this.data = changes.data.currentValue;
    this.key = changes.key.currentValue;
    this.changeCulturalImages();
    this.changeDesiredImages();
  }

}
