import {Component, OnInit} from '@angular/core';

@Component({
  selector: 'app-main',
  templateUrl: './main.component.html',
  styleUrls: ['./main.component.css']
})
export class MainComponent implements OnInit {

  clickedVectorData: any = {
    female: {
      heading: 'What values were chosen by',
      tag: 'female',
      question: 'The values chosen by <span style="color: #ffbc00;">females</span> from <span style=\'color: #ffbc00;\'>all</span> ages who live in <span style=\'color: #ffbc00;\'>Saudi Arabia</span>',
      personal_values: {
        list: ['positivity', 'ethics', 'friendship', 'patience', 'generosity', 'self-confidence', 'tolerance', 'adaptability',
          'equality', 'safety'],
        list_details: [
          {
            list_detail: 'A sense of optimism and assurance, with a willingness to respond to change.'
          },
          {
            list_detail: 'Actions guided by strong moral principles and a wish for equitable treatment.'
          },
          {
            list_detail: 'Focus on building close connections with others, demonstrating fortitude and showing\n' +
              'kindness to others.'
          },
          {
            list_detail: 'Attention to their personal welfare.'
          }
        ]
      },
      cultural_values: {
        list: ['Islamic principles', 'positivity', 'educational opportunities', 'law enforcement', 'concern for future generations',
          'equity and transparency', 'human rights', 'creativity', 'tradition', 'openness to the world'],
        list_details: [
          {
            list_detail: 'Adherence to religious teachings and the rule of law, while maintaining established\n' +
              'practices.'
          },
          {
            list_detail: 'A sense of optimism and the ability to promote new ideas, with a mindfulness to leave the\n' +
              'world a better place.'
          },
          {
            list_detail: 'Focus on upholding basic rights and freedoms, encouraging fair and open exchanges.'
          },
          {
            list_detail: 'Access to increase learning and breaking down barriers with other cultures.'
          }
        ]
      },
      desired_values: {
        list: ['educational opportunities', 'quality of life', 'financial stability', 'personal freedom', 'effective healthcare',
          'employment opportunities', 'positivity', 'concern for future generations', 'poverty reduction', 'peace'],
        list_details: [
          {
            list_detail: 'Greater emphasis on providing access to learning, jobs and medical care.'
          },
          {
            list_detail: 'A wish to increase happiness and harmony, and promote greater autonomy.'
          },
          {
            list_detail: 'Focus on building economic security while making efforts to support those in dire need.'
          },
          {
            list_detail: 'A can-do approach and a wish to leave a positive legacy.'
          }
        ]
      }
    },
    male: {
      heading: 'What values were chosen by',
      tag: 'male',
      question: 'The values chosen by <span style="color: #ffbc00;">males</span> from <span style=\'color: #ffbc00;\'>all</span> ages who live in <span style=\'color: #ffbc00;\'>Saudi Arabia</span>',
      personal_values: {
        list: ['positivity', 'patience', 'friendship', 'ethics', 'integrity', 'generosity', 'Islamic principles', 'faith',
          'respect', 'self-confidence'],
        list_details: [
          {
            list_detail: 'A sense of optimism and self-assurance.'
          },
          {
            list_detail: 'An appreciation for their closest connections, treating others with tolerance, consideration\n' +
              'and kindness.'
          },
          {
            list_detail: 'A strong sense of belief, with adherence to high moral principles and religious conventions.'
          }
        ]
      },
      cultural_values: {
        list: ['Islamic principles', 'ethics', 'tradition', 'governmental effectiveness', 'social cohesion', 'positivity',
          'caring for the disadvantaged', 'law enforcement', 'integrity', 'trust'],
        list_details: [
          {
            list_detail: 'Actions guided by religious teachings and moral standards, while adhering to the\n' +
              'rule of law.'
          },
          {
            list_detail: 'Maintaining established practices and a can-do approach.'
          },
          {
            list_detail: 'Strength in national administrative practices and ensuring those in need are looked after.'
          },
          {
            list_detail: 'Working to build interpersonal bonds and mutual confidence with others.'
          }
        ]
      },
      desired_values: {
        list: ['effective healthcare', 'educational opportunities', 'employment opportunities', 'peace', 'integrity',
          'financial stability', 'affordable housing', 'Islamic principles', 'positivity', 'ethics'],
        list_details: [
          {
            list_detail: 'Focus on providing access to jobs, learning and medical facilities, and reasonably priced\n' +
              'housing.'
          },
          {
            list_detail: 'Ongoing emphasis on moral standards and following religious teachings.'
          },
          {
            list_detail: 'Efforts to maintain optimism and find harmony.'
          },
          {
            list_detail: 'Attention to ensuring economic security.'
          }
        ]
      }
    },
    0: {
      heading: 'What values were chosen by',
      tag: '15 - 25',
      question: "The values chosen by <span style='color: #ffbc00;'>all Saudi's</span> from ages <span style='color: #ffbc00;'>15 - 25</span>  who live in <span style=\'color: #ffbc00;\'>Saudi Arabia</span>",
      personal_values: {
        list: ['friendship', 'positivity', 'patience', 'generosity', 'respect', 'ethics', 'self-confidence', 'integrity',
          'proficiency', 'safety'],
        list_details: [
          {
            list_detail: 'Appreciation for their closest connections, acting with kindness and consideration.'
          },
          {
            list_detail: 'A can-do, composed and self-assured approach.'
          },
          {
            list_detail: 'Focus on upholding high moral standards and demonstrating competence.'
          },
          {
            list_detail: 'Attention to their personal welfare.'
          }
        ]
      },
      cultural_values: {
        list: ['positivity', 'Islamic principles', 'equity and transparency', 'integrity', 'social cohesion', 'law enforcement',
          'concern for future generations', 'caring for the disadvantaged', 'tradition', 'openness to the world'],
        list_details: [
          {
            list_detail: 'A sense of optimism, with a wish to leave a positive legacy and look after those in need.'
          },
          {
            list_detail: 'Actions guided by religious conventions and established practices, while adhering to the\n' +
              'rule of law.'
          },
          {
            list_detail: 'Efforts to build strong bonds with others, acting openly with fairness and sincerity.'
          },
          {
            list_detail: 'Focus on breaking down barriers with other cultures.'
          }
        ]
      },
      desired_values: {
        list: ['educational opportunities', 'financial stability', 'quality of life', 'effective healthcare', 'positivity',
          'employment opportunities', 'equity and transparency', 'integrity', 'peace', 'complacency (L)'],
        list_details: [
          {
            list_detail: 'This group wants access to learning, jobs and medical facilities.'
          },
          {
            list_detail: 'They are looking to improve happiness and harmony, while building economic security.'
          },
          {
            list_detail: 'It is important to them to maintain an optimistic outlook while being comfortable with their\n' +
              'present lot in life.'
          },
          {
            list_detail: 'Citizens here want to continue to see principled interactions and ongoing focus on open\n' +
              'and just exchanges.'
          }
        ]
      }
    },
    1: {
      heading: 'What values were chosen by',
      tag: '25 - 35',
      question: "The values chosen by <span style='color: #ffbc00;'>all Saudi's</span> from ages <span style='color: #ffbc00;'>25 - 35</span>  who live in <span style=\'color: #ffbc00;\'>Saudi Arabia</span>",
      personal_values: {
        list: ['ethics', 'friendship', 'positivity', 'patience', 'self-confidence', 'generosity', 'integrity',
          'respect', 'health', 'humility'],
        list_details: [
          {
            list_detail: 'A strong sense of principle, treating others with consideration.'
          },
          {
            list_detail: 'Authenticity and showing kindness towards those closest to them.'
          },
          {
            list_detail: 'An optimistic outlook on life, with a proactive approach to their well - being.'
          },
          {
            list_detail: 'Faith in their skills and abilities, yet maintaining modesty and making time for others.'
          }
        ]
      },
      cultural_values: {
        list: ['Islamic principles', 'positivity', 'integrity', 'educational opportunities', 'tradition', 'law enforcement',
          'ethics', 'social cohesion', 'military strength', 'human rights', 'leadership'],
        list_details: [
          {
            list_detail: 'Actions guided by religious conventions, with people following established practices and\n' +
              'upholding the fundamental freedom of individuals.'
          },
          {
            list_detail: 'Steadfast adherence to rules and regulations, with forces protecting national security.'
          },
          {
            list_detail: 'Strong bonds across the nation and people guided by leaders following strong moral\n' +
              'principle.'
          },
          {
            list_detail: 'Learning pathways accessible to all and people demonstrating a can-do mentality.'
          }
        ]
      },
      desired_values: {
        list: ['financial stability', 'educational opportunities', 'employment opportunities', 'positivity', 'effective healthcare',
          'integrity', 'quality of life', 'peace', 'Islamic principles', 'poverty reduction'],
        list_details: [
          {
            list_detail: 'People asking for greater access to learning and jobs, ensuring their financial security and\n' +
              'reducing hardship.'
          },
          {
            list_detail: 'A call for adequate medical facilities, improving peoples’ sense of well -being and happiness.'
          },
          {
            list_detail: 'Focus on upholding tranquillity and leaning on religious teachings for guidance.'
          },
          {
            list_detail: 'An optimistic and principled outlook on life.'
          }
        ]
      }
    },
    2: {
      heading: 'What values were chosen by',
      tag: '35+',
      question: "The values chosen by <span style='color: #ffbc00;'>all Saudi's</span> from ages <span style='color: #ffbc00;'>35+</span>  who live in <span style=\'color: #ffbc00;\'>Saudi Arabia</span>",
      personal_values: {
        list: ['positivity', 'ethics', 'patience', 'integrity', 'generosity', 'friendship', 'faith', 'humility',
          'Islamic principles', 'proficiency'],
        list_details: [
          {
            list_detail: 'An upbeat approach, with focus on demonstrating competence and maintaining a sense of\n' +
              'modesty.'
          },
          {
            list_detail: 'Actions guided by a strong moral standards and religious beliefs.'
          },
          {
            list_detail: 'Appreciation for their closest connections, showing kindness and restraint.'
          }
        ]
      },
      cultural_values: {
        list: ['Islamic principles', 'tradition', 'law enforcement', 'educational opportunities', 'governmental effectiveness',
          'social cohesion', 'positivity', 'caring for the disadvantaged', 'peace', 'caring for the elderly'],
        list_details: [
          {
            list_detail: 'Productive administrative processes, with focus placed on looking after the aged and those\n' +
              'in need'
          },
          {
            list_detail: 'Actions and decisions guided by religious teachings and established conventions, while\n' +
              'upholding rules and regulations'
          },
          {
            list_detail: 'An optimistic outlook and space for everyone to learn'
          }
        ]
      },
      desired_values: {
        list: ['educational opportunities', 'effective healthcare', 'peace', 'employment opportunities', 'financial stability',
          'quality of life', 'Islamic principles', 'integrity', 'positivity', 'social cohesion'],
        list_details: [
          {
            list_detail: 'Greater emphasis on learning, with additional focus on increasing jobs and providing good\n' +
              'medical facilities'
          },
          {
            list_detail: 'Citizens want to continue to pull together, enjoying a greater sense of happiness and\n' +
              'tranquillity'
          },
          {
            list_detail: 'They want to maintain a can-do approach and build a strong economy'
          },
          {
            list_detail: 'It is important for people to act with sincerity and in accordance with religious beliefs'
          }
        ]
      }
    },
    south: {
      heading: 'What values were chosen by',
      tag: 'south',
      question: "The values chosen by <span style='color: #ffbc00;'>all Saudi's</span> from <span style='color: #ffbc00;'>all</span> ages who live in <span style=\'color: #ffbc00;\'>south</span>",
      personal_values: {
        list: ['patience', 'friendship', 'generosity', 'self-confidence', 'respect', 'safety', 'health', 'equity and transparency',
          'moderation', 'belonging'],
        list_details: [
          {
            list_detail: 'A considerate approach, with tolerance and kindness to others'
          },
          {
            list_detail: 'An appreciation for their attachments and closest connections'
          },
          {
            list_detail: 'A sense of self-assurance and restraint'
          },
          {
            list_detail: 'Focus on their own personal welfare and upholding just and open interactions'
          }
        ]
      },
      cultural_values: {
        list: ['social cohesion', 'leadership', 'military strength', 'concern for future generations', 'equity and transparency',
          'governmental effectiveness', 'Islamic principles', 'collaboration', 'positivity', 'human rights'],
        list_details: [
          {
            list_detail: 'Citizens working in partnership and building strong communal bonds'
          },
          {
            list_detail: 'A sense of optimism, with guidance and direction underpinned by adherence to religious\n' +
              'teachings and a wish to leave a positive legacy'
          },
          {
            list_detail: 'Strength in administrative processes and armed forces'
          },
          {
            list_detail: 'Focus on upholding basic rights and freedoms, and being open and fair'
          }
        ]
      },
      desired_values: {
        list: ['educational opportunities', 'effective healthcare', 'employment opportunities', 'quality of life',
          'concern for future generations', 'poverty reduction', 'positivity', 'collaboration', 'social cohesion', 'integrity'],
        list_details: [
          {
            list_detail: 'Services to provide jobs, learning and access to medical facilities'
          },
          {
            list_detail: 'Efforts to promote happiness in the population and minimise hardship'
          },
          {
            list_detail: 'Continued emphasis on building bonds and working closely together, with focus on leaving\n' +
              'a positive legacy'
          },
          {
            list_detail: 'Building a sense of optimism and sincerity'
          }
        ]
      }
    },
    east: {
      heading: 'What values were chosen by',
      tag: 'east',
      question: "The values chosen by <span style='color: #ffbc00;'>all Saudi's</span> from <span style='color: #ffbc00;'>all</span> ages who live in <span style=\'color: #ffbc00;\'>east</span>",
      personal_values: {
        list: ['ethics', 'positivity', 'equality', 'friendship', 'patience', 'experience', 'integrity', 'trust', 'safety',
          'self-confidence'],
        list_details: [
          {
            list_detail: 'An optimistic outlook on life, living by a strong set of principles'
          },
          {
            list_detail: 'An upright approach to relationships with those closest to them, treating everyone the\n' +
              'same'
          },
          {
            list_detail: 'A sense of security and placing confidence in others'
          },
          {
            list_detail: 'Faith in their own abilities, demonstrating skill and taking the time necessary to explain\n' +
              'and help others'
          }
        ]
      },
      cultural_values: {
        list: ['ethics', 'corruption', 'trust', 'affordable housing', 'aggression', 'caring for the disadvantaged', 'commitment',
          'acceptance', 'bureaucracy', 'employment opportunities'],
        list_details: [
          {
            list_detail: 'A society guided by strong moral standing'
          },
          {
            list_detail: 'People placing confidence in one another, however, this may at times, be tainted by some\n' +
              'people treating others with hostility'
          },
          {
            list_detail: 'Communities showing a dedication to assisting those less-fortunate, helping people with\n' +
              'low incomes obtain appropriate homes'
          },
          {
            list_detail: 'Measures in place to ensure a healthy job market'
          },
          {
            list_detail: 'Rigid and authoritarian processes with some taking advantage of the system for personal\n' +
              'gain'
          }
        ]
      },
      desired_values: {
        list: ['affordable housing', 'caring for the disadvantaged', 'commitment', 'employment opportunities', 'acceptance',
          'financial stability', 'complacency', 'ethics', 'positivity', 'trust'],
        list_details: [
          {
            list_detail: 'Ongoing dedication towards providing reasonably priced homes and showing concern for\n' +
              'those less fortunate'
          },
          {
            list_detail: 'A call for better access to jobs, allowing individuals to ensure basic economic security'
          },
          {
            list_detail: 'Confidence in others, enabling people to feel at ease and satisfied with life'
          }
        ]
      }
    },
    west: {
      heading: 'What values were chosen by',
      tag: 'west',
      question: "The values chosen by <span style='color: #ffbc00;'>all Saudi's</span> from <span style='color: #ffbc00;'>all</span> ages who live in <span style=\'color: #ffbc00;\'>west</span>",
      personal_values: {
        list: ['positivity', 'proficiency', 'integrity', 'humility', 'cooperation', 'discipline', 'ethics', 'courage',
          'trust', 'adaptability'],
        list_details: [
          {
            list_detail: 'A can-do, highly principled and unpretentious approach'
          },
          {
            list_detail: 'Inner-strength and competence in their endeavours'
          },
          {
            list_detail: 'Supportive interactions to build mutual confidence with others'
          },
          {
            list_detail: 'A sense of fearlessness and openness to change'
          }
        ]
      },
      cultural_values: {
        list: ['creativity', 'dialogue', 'positivity', 'ethics', 'covering for others', 'trust', 'personal freedom', 'wisdom',
          'peace', 'equity and transparency'],
        list_details: [
          {
            list_detail: 'Highly insightful and imaginative approach'
          },
          {
            list_detail: 'Open exchanges, where people support one another and build mutual confidence'
          },
          {
            list_detail: 'A sense of optimism and harmony, while promoting individual independence'
          },
          {
            list_detail: 'Actions guided by strong moral principles and equitable treatment'
          }
        ]
      },
      desired_values: {
        list: ['quality of life', 'educational opportunities', 'personal freedom', 'financial stability', 'peace',
          'effective healthcare', 'complacency', 'positivity', 'integrity', 'equity and transparency'],
        list_details: [
          {
            list_detail: 'A wish to improve happiness, remaining content and unified'
          },
          {
            list_detail: 'Focus on increasing learning and providing good medical facilities, while building economic\n' +
              'security'
          },
          {
            list_detail: 'Ongoing attention to increasing autonomy and upholding a sense of optimism'
          },
          {
            list_detail: 'People acting with honour and maintaining an open and just approach'
          }
        ]
      }
    },
    north: {
      heading: 'What values were chosen by',
      tag: 'north',
      question: "The values chosen by <span style='color: #ffbc00;'>all Saudi's</span> from <span style='color: #ffbc00;'>all</span> ages who live in <span style=\'color: #ffbc00;\'>north</span>",
      personal_values: {
        list: ['friendship', 'generosity', 'ethics', 'patience', 'self-confidence', 'safety', 'tolerance', 'experience',
          'equality', 'health'],
        list_details: [
          {
            list_detail: 'A sense of assurance, with efforts to ensure competence and know-how in their approach'
          },
          {
            list_detail: 'Appreciation for their close connections, demonstrating acceptance and kindness towards\n' +
              'others'
          },
          {
            list_detail: 'Ensuring their actions are guided by moral principles, with a desire to ensure fair\n' +
              'treatment for all'
          },
          {
            list_detail: 'Attention to their personal welfare'
          }
        ]
      },
      cultural_values: {
        list: ['integrity', 'positivity', 'freedom of expression', 'human rights', 'leadership', 'law enforcement',
          'equity and transparency', 'innovation', 'respect', 'tolerance'],
        list_details: [
          {
            list_detail: 'Actions guided by personal standards and adherence to legal rules and regulations'
          },
          {
            list_detail: 'Striving to treat people with consideration and acceptance, and ensuring all have access to\n' +
              'basic civil liberties'
          },
          {
            list_detail: 'The ability to openly share their ideas and thoughts, supported by a just and direct\n' +
              'approach'
          },
          {
            list_detail: 'People providing guidance to others and encouraging the introduction of new approaches'
          }
        ]
      },
      desired_values: {
        list: ['integrity', 'tolerance', 'positivity', 'respect', 'financial stability', 'equity and transparency',
          'Islamic principles', 'complacency', 'human rights', 'quality of life'],
        list_details: [
          {
            list_detail: 'People want to see actions guided by religious teachings and sincerity, and encourage a\n' +
              'more open and fair society'
          },
          {
            list_detail: 'There is a call for economic security and an increase in their sense of happiness'
          },
          {
            list_detail: 'They would like to ensure all people have access to basic civil liberties and are shown\n' +
              'consideration and acceptance'
          },
          {
            list_detail: 'Citizens would like to maintain an optimistic outlook and foster a sense of comfort with\n' +
              'what they currently have'
          }
        ]
      }
    },
    center: {
      heading: 'What values were chosen by',
      tag: 'center',
      question: "The values chosen by <span style='color: #ffbc00;'>all Saudi's</span> from <span style='color: #ffbc00;'>all</span> ages who live in <span style=\'color: #ffbc00;\'>central region</span>",
      personal_values: {
        list: ['patience', 'ethics', 'friendship', 'generosity', 'positivity', 'self-confidence', 'gratitude', 'humour/ fun',
          'respect', 'compassion'],
        list_details: [
          {
            list_detail: 'Self-assurance, with a tolerant and appreciative approach'
          },
          {
            list_detail: 'A light-hearted, can-do outlook, with actions guided by high moral principles'
          },
          {
            list_detail: 'Focus on their closest connections, demonstrating consideration, benevolence and\n' +
              'kindness'
          }
        ]
      },
      cultural_values: {
        list: ['Islamic principles', 'tradition', 'social cohesion', 'law enforcement', 'materialistic', 'helpfulness', 'peace',
          'caring for the elderly', 'governmental effectiveness', 'educational opportunities'],
        list_details: [
          {
            list_detail: 'Actions guided by religious conventions and established practices'
          },
          {
            list_detail: 'People pulling together and supporting one another, but with some over -emphasis on\n' +
              'increasing possessions'
          },
          {
            list_detail: 'Efforts to maintain harmony and uphold rules and regulations'
          },
          {
            list_detail: 'Strength in national administrative practices, with provisions to increase learning and look\n' +
              'after the aged'
          }
        ]
      },
      desired_values: {
        list: ['Islamic principles', 'peace', 'employment opportunities', 'financial stability', 'effective healthcare',
          'law enforcement', 'poverty reduction', 'educational opportunities', 'positivity', 'social cohesion'],
        list_details: [
          {
            list_detail: 'Continued emphasis on upholding religious teachings, while maintaining the rule of law\n' +
              'and providing more economic security'
          },
          {
            list_detail: 'Efforts to minimise hardship and provide access to jobs and medical facilities'
          },
          {
            list_detail: 'Ongoing focus on building bonds in the community and encouraging learning'
          },
          {
            list_detail: 'A more optimistic outlook and a greater sense of tranquility'
          }
        ]
      }
    },
    male_0: {
      question: 'The values chosen by all Saudi <span style="color: #ffbc00;">male</span> from the age <span style="color: #ffbc00">15 - 25</span> who live in <span style="color: #ffbc00">Saudi Arabia</span>',
      personal_values: {
        list: ['Positive', 'The friendship', 'The vineyard', 'Patience', 'Respect', 'Honesty', 'Honor and morality', 'Capacity chest',
          'Certainty and extent of faith', 'Workmanship']
      },
      cultural_values: {
        list: ['Honesty', 'Islamic principles', 'Family bonding', 'Positive', 'Honor and morality', 'Take care of those in need',
          'Effective Government', 'Tradition', 'Mercy', 'law enforcement']
      },
      desired_values: {
        list: ['Honesty', 'Educational opportunities', 'job opportunities', 'Healthcare', 'Positive', 'Peace', 'Financial stability',
          'Quality of Life', 'Provide housing', 'Complacency']
      },
      total: 670
    },
    male_1: {
      question: 'The values chosen by <span style="color: #ffbc00;">males</span> from the age <span style="color: #ffbc00">25 - 35</span> who live in <span style="color: #ffbc00">Saudi Arabia</span>',
      personal_values: {
        list: ['Honor and morality', 'The friendship', 'Positive', 'Patience', 'self confidence', 'The vineyard', ' Honesty', 'Respect',
          'Islamic principles', 'Certainty and extent of faith']
      },
      cultural_values: {
        list: ['Honesty', 'Islamic principles', 'Positive', 'Family bonding', 'Honor and morality', 'Military force', 'Traditions',
          'Effective Government', 'Confidence', 'law enforcement'
        ]
      },
      desired_values: {
        list: ['Honesty', 'Positive', 'Financial stability', 'Provide housing', 'Healthcare', 'Islamic principles', 'job opportunities',
          'Educational opportunities', 'Honor and morality', 'Peace']
      },
      total: 535
    },
    male_2: {
      question: 'The values chosen by <span style="color: #ffbc00;">males</span> from the age <span style="color: #ffbc00">35+</span> who live in <span style="color: #ffbc00">Saudi Arabia</span>',
      personal_values: {
        list: ['Positive', ' Honesty', 'Patience', 'Islamic principles', 'Honor and morality', 'The vineyard', 'Workmanship',
          'Certainty and extent of faith', 'Discipline', 'Humility']
      },
      cultural_values: {
        list: ['Islamic principles', 'Honesty', 'Family bonding', 'Traditions', 'Honor and morality', 'Effective Government',
          'law enforcement', 'Take care of those in need', 'Positive', 'Care of the elderly'
        ]
      },
      desired_values: {
        list: ['Honesty', 'Peace', 'Healthcare', 'Islamic principles', 'Provide housing', 'Financial stability', 'Positive',
          'Educational opportunities', 'job opportunities', 'Family bonding']
      },
      total: 823
    },
    center_male: {
      question: 'The values chosen by <span style="color: #ffbc00;">males</span> from <span style="color: #ffbc00">all</span> ages who live in <span style="color: #ffbc00">central region</span>',
      personal_values: {
        list: ['Patience', 'Honor and morality', 'The vineyard', 'The friendship', 'Capacity chest', 'Positive', 'Gratitude and thanks',
          'Cooperate', 'Mercy', 'Certainty and extent of faith']
      },
      cultural_values: {
        list: ['Islamic principles', 'Family bonding', 'Traditions', 'Love offense systems / bucking know', 'Love materialism', 'Aelovesah',
          'Waste of resources / overkill', 'Care of the elderly', 'law enforcement', 'Peace'
        ]
      },
      desired_values: {
        list: ['Peace', 'Honesty', 'Islamic principles', 'Healthcare', 'Financial stability', 'law enforcement',
          'Tolerance and forgiveness', 'Positive', 'Prosperity', 'job opportunities']
      },
      total: 564
    },
    east_male: {
      question: 'The values chosen by <span style="color: #ffbc00;">males</span> from <span style="color: #ffbc00">all</span> ages who live in <span style="color: #ffbc00">east</span>',
      personal_values: {
        list: ['Positive', ' Innovation', 'Professional growth Professional by', 'Financial Stability', 'integrity', 'Risk',
          'Logic', 'Quality of Life', 'Honesty', 'Enthusiasm']
      },
      cultural_values: {
        list: ['Honor and morality', 'Confidence', 'Bureaucracy', 'Agressive behavior', 'Commitment', 'Provide housing',
          'Corruption', 'Take care of those in need', 'acceptance of the other', 'Wisdom'
        ]
      },
      desired_values: {
        list: ['Provide housing', 'Commitment', 'Confidence', 'Agressive behavior', 'Bureaucracy', 'Take care of those in need',
          'Honor and morality', 'acceptance of the other', 'Corruption', 'Equity']
      },
      total: 321
    },
    west_male: {
      question: 'The values chosen by <span style="color: #ffbc00;">males</span> from <span style="color: #ffbc00">all</span> ages who live in <span style="color: #ffbc00">west</span>',
      personal_values: {
        list: ['Positive', ' Workmanship', ' Honesty', 'Confidence', 'Islamic principles', 'Certainty and extent of faith', 'Discipline',
          'Advise others well', 'Honor and morality', 'The friendship']
      },
      cultural_values: {
        list: ['Honesty', 'Confidence', 'Positive', 'Honor and morality', 'Wisdom', 'Awareness of values', 'Mercy', 'Patience',
          'Justice and Transparency', 'Islamic principles'
        ]
      },
      desired_values: {
        list: ['Honesty', 'Honor and morality', 'Confidence', 'Educational opportunities', 'Complacency', 'Positive', 'Wisdom',
          'Financial stability', 'Healthcare', 'Mercy']
      },
      total: 593
    },
    north_male: {
      question: 'The values chosen by <span style="color: #ffbc00;">males</span> from <span style="color: #ffbc00">all</span> ages who live in <span style="color: #ffbc00">north</span>',
      personal_values: {
        list: ['The friendship', 'The vineyard', 'Honor and morality', 'Experience', 'Equality', 'Patience', 'Tolerance and forgiveness',
          'the health', 'self confidence', 'Safety']
      },
      cultural_values: {
        list: ['Honesty', 'Positive', 'Leadership', 'human rights', 'freedom of expression', 'Justice and Transparency', 'law enforcement',
          'Innovation', 'Openness to the world', 'Respect'
        ]
      },
      desired_values: {
        list: ['Honesty', 'Justice and Transparency', 'Positive', 'Tolerance and forgiveness', 'Respect', 'Financial stability',
          'human rights', 'Complacency', 'Innovation', 'Islamic principles']
      },
      total: 187
    },
    south_male: {
      question: 'The values chosen by <span style="color: #ffbc00;">males</span> from <span style="color: #ffbc00">all</span> ages who live in <span style="color: #ffbc00">south</span>',
      personal_values: {
        list: ['Patience', 'The vineyard', 'The friendship', 'Respect',
          'self confidence', ' Honesty', 'Positive', 'Moderation', 'Humility',
          'Justice']
      },
      cultural_values: {
        list: ['Military force', 'Effective Government', 'Leadership',
          'Islamic principles', 'Family bonding', 'law enforcement',
          'Interest in the next generation', 'Justice and Transparency',
          'integrity', 'Waste of resources / overkill']
      },
      desired_values: {
        list: ['Educational opportunities', 'Healthcare', 'job opportunities',
          'The fight against poverty', 'Quality of Life', 'Family bonding',
          'Provide housing', 'Interest in the next generation',
          'Financial stability', 'Positive']
      },
      total: 363
    },
    female_0: {
      question: 'The values chosen by <span style="color: #ffbc00;">females</span> from the age <span style="color: #ffbc00">15 - 25</span> who live in <span style="color: #ffbc00">Saudi Arabia</span>',
      personal_values: {
        list: ['The friendship', 'Positive', 'Patience', 'self confidence', 'Honor and morality', 'The vineyard', 'Respect',
          'Adapt to variables', 'Honesty', 'Tolerance and forgiveness']
      },
      cultural_values: {
        list: ['Honesty', 'Positive', 'Justice and Transparency', 'creativity', 'Openness to the world', 'Family bonding',
          'law enforcement', 'Educational opportunities', 'human rights', 'freedom of expression'
        ]
      },
      desired_values: {
        list: ['Educational opportunities', 'Honesty', 'Financial stability', 'Positive', 'Quality of Life', 'Justice and Transparency',
          'Healthcare', 'personal freedom', 'The fight against poverty', 'Tolerance and forgiveness']
      },
      total: 637
    },
    female_1: {
      question: 'The values chosen by <span style="color: #ffbc00;">females</span> from the age <span style="color: #ffbc00">25 - 35</span> who live in <span style="color: #ffbc00">Saudi Arabia</span>',
      personal_values: {
        list: ['Positive', 'Honor and morality', 'The friendship', 'Patience', 'self confidence', 'The vineyard', 'Equality',
          ' Workmanship', 'Capacity chest', 'Humility']
      },
      cultural_values: {
        list: ['Honesty', 'Positive', 'Educational opportunities', 'Islamic principles', 'Interest in the next generation',
          'freedom of expression', 'law enforcement', 'creativity', 'Openness to the world', 'Healthcare'
        ]
      },
      desired_values: {
        list: ['Honesty', 'Financial stability', 'Quality of Life', 'Educational opportunities', 'personal freedom', 'job opportunities',
          'Positive', 'Family bonding', 'Peace', 'Healthcare']
      },
      total: 560
    },
    female_2: {
      question: 'The values chosen by <span style="color: #ffbc00;">females</span> from the age <span style="color: #ffbc00">35+</span> who live in <span style="color: #ffbc00">Saudi Arabia</span>',
      personal_values: {
        list: ['Positive', 'Honor and morality', 'Patience', 'The friendship', 'The vineyard', ' Capacity chest',
          'Tolerance and forgiveness', 'Workmanship', 'Honesty', 'Humility']
      },
      cultural_values: {
        list: ['Islamic principles', 'Honesty', 'Educational opportunities', 'Family bonding', 'law enforcement', 'Positive', 'Traditions',
          'Peace', 'human rights', 'Interest in the next generation'
        ]
      },
      desired_values: {
        list: ['Honesty', 'Educational opportunities', 'Quality of Life', 'job opportunities', 'personal freedom', 'Financial stability',
          'Healthcare', 'Family bonding', 'Interest in the next generation', 'Positive']
      },
      total: 776
    },
    center_female: {
      question: 'The values chosen by <span style="color: #ffbc00;">females</span> from <span style="color: #ffbc00">all</span> ages who live in <span style="color: #ffbc00">central region</span>',
      personal_values: {
        list: ['The friendship', 'Patience', 'Honor and morality', 'Positive', 'self confidence', 'Respect', 'The vineyard',
          'Gratitude and thanks', 'Tolerance and forgiveness', 'Capacity chest']
      },
      cultural_values: {
        list: ['Islamic principles', 'law enforcement', 'Traditions', 'human rights', 'Honesty', 'Positive', 'Openness to the world',
          'Love materialism', 'Family bonding', 'The unemployment'
        ]
      },
      desired_values: {
        list: ['job opportunities', 'Honesty', 'The fight against poverty', 'Family bonding', 'Islamic principles', 'Financial stability',
          'Educational opportunities', 'Interest in the next generation', 'Positive', 'Justice and Transparency']
      },
      total: 535
    },
    east_female: {
      question: 'The values chosen by <span style="color: #ffbc00;">females</span> from <span style="color: #ffbc00">all</span> ages who live in <span style="color: #ffbc00">east</span>',
      personal_values: {
        list: ['Honor and morality', 'Equality', 'The friendship', 'Patience', 'Experience', 'The vineyard', 'self confidence',
          'Tolerance and forgiveness', 'Safety', 'Positive']
      },
      cultural_values: {
        list: ['Honesty', 'Positive', 'freedom of expression', 'Fear of the future', 'Tolerance and forgiveness',
          'Ridicule others / Bullying', 'law enforcement', 'Educational opportunities', 'Financial stability', 'parasitism'
        ]
      },
      desired_values: {
        list: ['Honesty', 'Financial stability', 'Tolerance and forgiveness', 'personal freedom', 'Positive', 'Quality of Life',
          'Complacency', 'Educational opportunities', 'Islamic principles', 'job opportunities']
      },
      total: 295
    },
    west_female: {
      question: 'The values chosen by <span style="color: #ffbc00;">females</span> from <span style="color: #ffbc00">all</span> ages who live in <span style="color: #ffbc00">west</span>',
      personal_values: {
        list: ['Positive', ' Workmanship', 'Cooperate', 'Help learning / care and provide advice and guidance', ' Honesty',
          'Courage', 'Humility', 'To be the best', 'integrity', 'Capacity chest']
      },
      cultural_values: {
        list: ['creativity', 'personal freedom', 'Dialogue', 'Jackets on your brother', 'Peace', 'Diversity', 'Educational opportunities',
          'Take care of those in need', 'Honesty', 'Positive'
        ]
      },
      desired_values: {
        list: ['Quality of Life', 'personal freedom', 'Peace', 'Financial stability', 'Educational opportunities', 'Honesty', 'Healthcare',
          'job opportunities', 'Government services, reliable', 'Family bonding']
      },
      total: 583
    },
    north_female: {
      question: 'The values chosen by <span style="color: #ffbc00;">females</span> from <span style="color: #ffbc00">all</span> ages who live in <span style="color: #ffbc00">north</span>',
      personal_values: {
        list: ['The friendship', 'The vineyard', 'Patience', 'Honor and morality', 'self confidence', 'Safety',
          'Tolerance and forgiveness', 'Justice', 'personal freedom', 'Positive']
      },
      cultural_values: {
        list: ['Honesty', 'law enforcement', 'Positive', 'freedom of expression', 'Justice and Transparency', 'Tolerance and forgiveness',
          'The unemployment', 'human rights', 'Islamic principles', 'Leadership'
        ]
      },
      desired_values: {
        list: ['Honesty', 'Respect', 'Islamic principles', 'Tolerance and forgiveness', 'Positive', 'Nature Conservation', 'Complacency',
          'Financial stability', 'Quality of Life', 'Take care of those in need']
      },
      total: 186
    },
    south_female: {
      question: 'The values chosen by <span style="color: #ffbc00;">females</span> from <span style="color: #ffbc00">all</span> ages who live in <span style="color: #ffbc00">south</span>',
      personal_values: {
        list: ['Safety', 'The friendship', 'Patience', 'the health', 'self confidence',
          'The vineyard', 'Justice', 'Experience', 'Adapt to variables',
          'Respect']
      },
      cultural_values: {
        list: ['Family bonding', 'Blurred vision', 'Educational opportunities',
          'Justice and Transparency', 'Cooperat', 'Positive',
          'Interest in the next generation', 'human rights', 'Animal Care',
          'The unemployment']
      },
      desired_values: {
        list: ['Educational opportunities', 'Quality of Life',
          'Interest in the next generation', 'Healthcare', 'Positive', 'Cooperat',
          'The fight against poverty', 'Justice and Transparency', 'Animal Care',
          'Family bonding']
      },
      total: 374
    },
    center_0: {
      question: 'The values chosen by <span style="color: #ffbc00;">all Saudi\'s</span> from the age <span style="color: #ffbc00">15 - 25</span> who live in <span style="color: #ffbc00">central region</span>',
      personal_values: {
        list: ['Patience', 'The friendship', 'Respect', 'The vineyard', 'Positive', 'self confidence', 'Mercy', 'Honor and morality',
          'Honesty', 'Capacity chest']
      },
      cultural_values: {
        list: ['Family bonding', 'Honesty', 'Islamic principles', 'Love materialism', 'Tribal fanaticism', 'The unemployment',
          'Ridicule others / Bullying', 'Discrimination by sex', 'Traditions', 'Aelovesah'
        ]
      },
      desired_values: {
        list: ['Honesty', 'Peace', 'Educational opportunities', 'Islamic principles', 'Financial stability', 'The fight against poverty',
          'job opportunities', 'Healthcare', 'Positive', 'Justice and Transparency']
      },
      total: 363
    },
    east_0: {
      question: 'The values chosen by <span style="color: #ffbc00;">all Saudi\'s</span> from the age <span style="color: #ffbc00">15 - 25</span> who live in <span style="color: #ffbc00">east</span>',
      personal_values: {
        list: ['Positive', 'Honor and morality', 'Equality', 'The friendship', 'Discipline', 'Experience', 'Confidence',
          'personal freedom', 'Safety', 'self confidence']
      },
      cultural_values: {
        list: ['Honor and morality', 'Corruption', 'acceptance of the other', 'Provide housing', 'Confidence', 'Agressive behavior',
          'Take care of those in need', 'Bureaucracy', 'Positive', 'Commitment'
        ]
      },
      desired_values: {
        list: ['Positive', 'Honesty', 'Financial stability', 'job opportunities', 'Provide housing', 'Take care of those in need',
          'acceptance of the other', 'Agressive behavior', 'Confidence', 'Commitment']
      },
      total: 206
    },
    west_0: {
      question: 'The values chosen by <span style="color: #ffbc00;">all Saudi\'s</span> from the age <span style="color: #ffbc00">15 - 25</span> who live in <span style="color: #ffbc00">west</span>',
      personal_values: {
        list: ['Positive', ' Workmanship', ' Honesty', 'Humility', 'Adapt to variables', 'Discipline', ' Capacity chest', 'Confidence',
          'The friendship', 'Courage']
      },
      cultural_values: {
        list: ['Jackets on your brother', 'Dialogue', 'Honesty', 'creativity', 'Confidence', 'Honor and morality', 'Positive', 'Mercy',
          'Justice and Transparency', 'Take care of those in need'
        ]
      },
      desired_values: {
        list: ['Quality of Life', 'Educational opportunities', 'personal freedom', 'Honesty', 'Financial stability', 'Peace', 'Complacency',
          'Healthcare', 'Justice and Transparency', 'Confidence']
      },
      total: 370
    },
    north_0: {
      question: 'The values chosen by <span style="color: #ffbc00;">all Saudi\'s</span> from the age <span style="color: #ffbc00">15 - 25</span> who live in <span style="color: #ffbc00">north</span>',
      personal_values: {
        list: ['The friendship', 'The vineyard', 'Patience', 'Honor and morality', 'self confidence', 'Safety',
          'Tolerance and forgiveness', 'Experience', 'the health', 'Justice']
      },
      cultural_values: {
        list: ['Honesty', 'Positive', 'Justice and Transparency', 'Respect', 'Openness to the world', 'human rights', 'Innovation',
          'Leadership', 'Tolerance and forgiveness', 'law enforcement'
        ]
      },
      desired_values: {
        list: ['Honesty', 'Positive', 'Tolerance and forgiveness', 'Financial stability', 'Justice and Transparency', 'Respect',
          'human rights', 'Complacency', 'Take care of those in need', 'Quality of Life']
      },
      total: 129
    },
    south_0: {
      question: 'The values chosen by <span style="color: #ffbc00;">all Saudi\'s</span> from the age <span style="color: #ffbc00">15 - 25</span> who live in <span style="color: #ffbc00">south</span>',
      personal_values: {
        list: ['The friendship', 'Respect', 'Patience', 'self confidence',
          'The vineyard', 'Justice', 'The initiative', 'Safety', 'Moderation',
          'Positive']
      },
      cultural_values: {
        list: ['Blurred vision', 'Leadership', 'Justice and Transparency',
          'law enforcement', 'Military force', 'Effective Government', 'Positive',
          'Islamic principles', 'Interest in the next generation',
          'Family bonding']
      },
      desired_values: {
        list: ['Educational opportunities', 'Healthcare', 'job opportunities',
          'Quality of Life', 'Positive', 'Justice and Transparency',
          'The fight against poverty', 'Cooperat', 'Financial stability',
          'Complacency']
      },
      total: 239
    },
    center_1: {
      question: 'The values chosen by <span style="color: #ffbc00;">all Saudi\'s</span> from the age <span style="color: #ffbc00">25 - 35</span> who live in <span style="color: #ffbc00">central region</span>',
      personal_values: {
        list: ['Honor and morality', 'The friendship', 'Patience', 'self confidence', 'Positive', 'Gratitude and thanks', 'The vineyard',
          'Capacity chest', 'Certainty and extent of faith', 'Tolerance and forgiveness']
      },
      cultural_values: {
        list: ['Islamic principles', 'Family bonding', 'Traditions', 'Honesty', 'Love materialism', 'Aelovesah', 'Peace', 'law enforcement',
          'Positive', 'Effective Government'
        ]
      },
      desired_values: {
        list: ['Honesty', 'Islamic principles', 'Financial stability', 'Peace', 'job opportunities', 'Healthcare', 'law enforcement',
          'Positive', 'The fight against poverty', 'Family bonding']
      },
      total: 305
    },
    east_1: {
      question: 'The values chosen by <span style="color: #ffbc00;">all Saudi\'s</span> from the age <span style="color: #ffbc00">25 - 35</span> who live in <span style="color: #ffbc00">east</span>',
      personal_values: {
        list: ['Honor and morality', 'Equality', 'The friendship', 'Patience', 'Positive', 'Experience', 'Self-realization', 'Workmanship',
          'the health', 'self confidence']
      },
      cultural_values: {
        list: ['Honesty', 'Honor and morality', 'Confidence', 'Positive', 'Corruption', 'Take care of those in need', 'Provide housing',
          'Commitment', 'Dialogue', 'Fear of the future'
        ]
      },
      desired_values: {
        list: ['Provide housing', 'Honesty', 'Financial stability', 'Complacency', 'Positive', 'Equity', 'Tolerance and forgiveness',
          'Commitment', 'personal freedom', 'Take care of those in need']
      },
      total: 178
    },
    west_1: {
      question: 'The values chosen by <span style="color: #ffbc00;">all Saudi\'s</span> from the age <span style="color: #ffbc00">25 - 35</span> who live in <span style="color: #ffbc00">west</span>',
      personal_values: {
        list: ['Positive', ' Workmanship', ' Honesty', 'Honor and morality', 'Humility', 'Discipline', 'Confidence', 'Cooperate',
          'Capacity chest', 'Help learning / care and provide advice and guidance']
      },
      cultural_values: {
        list: ['Honesty', 'Positive', 'Honor and morality', 'personal freedom', 'Confidence', 'creativity', 'Wisdom',
          'Educational opportunities', 'Islamic principles', 'Awareness of values'
        ]
      },
      desired_values: {
        list: ['Honesty', 'Financial stability', 'Positive', 'Educational opportunities', 'Quality of Life', 'Peace',
          'personal freedom', 'Honor and morality', 'Healthcare', 'Family bonding']
      },
      total: 308
    },
    north_1: {
      question: 'The values chosen by <span style="color: #ffbc00;">all Saudi\'s</span> from the age <span style="color: #ffbc00">25 - 35</span> who live in <span style="color: #ffbc00">north</span>',
      personal_values: {
        list: ['The friendship', 'Honor and morality', 'The vineyard', 'Patience', 'self confidence', 'Equality',
          'Tolerance and forgiveness', 'Respect', 'Justice', 'Safety']
      },
      cultural_values: {
        list: ['Honesty', 'Leadership', 'freedom of expression', 'human rights', 'law enforcement', 'Positive', 'The unemployment',
          'Innovation', 'Justice and Transparency', 'Cooperat'
        ]
      },
      desired_values: {
        list: ['Honesty', 'Respect', 'Justice and Transparency', 'Islamic principles', 'Financial stability', 'Tolerance and forgiveness',
          'human rights', 'Quality of Life', 'Positive', 'Complacency']
      },
      total: 104
    },
    south_1: {
      question: 'The values chosen by <span style="color: #ffbc00;">all Saudi\'s</span> from the age <span style="color: #ffbc00">25 - 35</span> who live in <span style="color: #ffbc00">south</span>',
      personal_values: {
        list: ['Patience', 'The friendship', 'The vineyard', 'self confidence',
          'Safety', 'the health', 'Adapt to variables', 'Respect', 'Moderation',
          'Volunteering']
      },
      cultural_values: {
        list: ['Military force', 'Family bonding', 'Educational opportunities',
          'Cooperate', 'human rights', 'Interest in the next generation',
          'Positive', 'Healthcare', 'Effective Government', 'Leadership']
      },
      desired_values: {
        list: ['Educational opportunities', 'Interest in the next generation',
          'Healthcare', 'job opportunities', 'The fight against poverty',
          'Positive', 'Quality of Life', 'Family bonding', 'Cooperat',
          'Animal Care']
      },
      total: 200
    },
    center_2: {
      question: 'The values chosen by <span style="color: #ffbc00;">all Saudi\'s</span> from the age <span style="color: #ffbc00">35+</span> who live in <span style="color: #ffbc00">central region</span>',
      personal_values: {
        list: ['Honor and morality', 'Patience', 'Positive', 'The vineyard', 'Islamic principles', 'Careful reputation',
          'Gratitude and thanks', 'Certainty and extent of faith', 'The friendship', ' Capacity chest']
      },
      cultural_values: {
        list: ['Islamic principles', 'Traditions', 'Family bonding', 'law enforcement', 'Care of the elderly', 'Educational opportunities',
          'Peace', 'Love materialism', 'human rights', 'Effective Government'
        ]
      },
      desired_values: {
        list: ['Islamic principles', 'Peace', 'Honesty', 'Family bonding', 'Healthcare', 'job opportunities', 'Provide housing', 'Positive',
          'Financial stability', 'law enforcement']
      },
      total: 431
    },
    east_2: {
      question: 'The values chosen by <span style="color: #ffbc00;">all Saudi\'s</span> from the age <span style="color: #ffbc00">35+</span> who live in <span style="color: #ffbc00">east</span>',
      personal_values: {
        list: ['Positive', 'Honor and morality', ' Honesty', 'Equality', 'Patience', 'Confidence', 'Safety', 'Tolerance and forgiveness',
          'Risk', 'The friendship']
      },
      cultural_values: {
        list: ['Commitment', 'Corruption', 'Agressive behavior', 'Honor and morality', 'Confidence', 'Provide housing',
          'Take care of those in need', 'job opportunities', 'Bureaucracy', 'Wisdom'
        ]
      },
      desired_values: {
        list: ['Provide housing', 'Honesty', 'Commitment', 'acceptance of the other', 'Take care of those in need', 'job opportunities',
          'Confidence', 'Honor and morality', 'Mercy', 'Complacency']
      },
      total: 232
    },
    west_2: {
      question: 'The values chosen by <span style="color: #ffbc00;">all Saudi\'s</span> from the age <span style="color: #ffbc00">35+</span> who live in <span style="color: #ffbc00">west</span>',
      personal_values: {
        list: ['Positive', ' Workmanship', 'Honesty', 'Cooperate', 'Discipline', 'Adapt to variables', 'Courage', 'Honor and morality',
          'Humility', 'Determination and perseverance']
      },
      cultural_values: {
        list: ['Honesty', 'Dialogue', 'creativity', 'Positive', 'personal freedom', 'Justice and Transparency', 'Peace',
          'Awareness of values', 'Jackets on your brother', 'Wisdom'
        ]
      },
      desired_values: {
        list: ['Quality of Life', 'Honesty', 'Educational opportunities', 'personal freedom', 'Financial stability', 'Peace', 'Healthcare',
          'Complacency', 'Positive', 'Justice and Transparency']
      },
      total: 498
    },
    north_2: {
      question: 'The values chosen by <span style="color: #ffbc00;">all Saudi\'s</span> from the age <span style="color: #ffbc00">35+</span> who live in <span style="color: #ffbc00">north</span>',
      personal_values: {
        list: ['The vineyard', 'The friendship', 'Patience', 'Honor and morality', 'Experience', 'Safety', 'self confidence', 'the health',
          'Tolerance and forgiveness', 'Equality']
      },
      cultural_values: {
        list: ['Honesty', 'freedom of expression', 'law enforcement', 'Positive', 'human rights', 'Leadership', 'Tolerance and forgiveness',
          'Islamic principles', 'Innovation', 'Justice and Transparency'
        ]
      },
      desired_values: {
        list: ['Honesty', 'Tolerance and forgiveness', 'Positive', 'Respect', 'Islamic principles', 'Complacency',
          'Justice and Transparency', 'Financial stability', 'human rights', 'Interest in the next generation']
      },
      total: 140
    },
    south_2: {
      question: 'The values chosen by <span style="color: #ffbc00;">all Saudi\'s</span> from the age <span style="color: #ffbc00">35+</span> who live in <span style="color: #ffbc00">south</span>',
      personal_values: {
        list: ['Patience', 'The vineyard', 'The friendship', 'the health', 'Safety',
          'Affiliation', 'Respect', 'self confidence', 'Experience', 'Humility']
      },
      cultural_values: {
        list: ['Family bonding', 'Justice and Transparency',
          'Interest in the next generation', 'Leadership', 'Volunteering',
          'Blurred vision', 'Effective Government',
          'Waste of resources / overkill', 'Tribal fanaticism',
          'Islamic principles']
      },
      desired_values: {
        list: ['Educational opportunities', 'Healthcare', 'Quality of Life',
          'Interest in the next generation', 'job opportunities',
          'Family bonding', 'The fight against poverty', 'Positive', 'Cooperat',
          'Justice and Transparency']
      },
      total: 298
    },
    center_male_0: {
      question: 'The values chosen by <span style="color: #ffbc00;">males</span> from ages <span style="color: #ffbc00;"> 15 - 25 </span> who live in <span style="color: #ffbc00;"> central region </span>',
      personal_values: {
        list: ['Patience', 'The vineyard', 'Positive', 'The friendship', 'Capacity chest', 'Respect', 'Honor and morality',
          'Gratitude and thanks', 'Mercy', 'Cooperate']
      },
      cultural_values: {
        list: ['Family bonding', 'Islamic principles', 'Love materialism', 'Love offense systems / bucking know', 'Honesty', 'Aelovesah',
          'Tribal fanaticism', 'Traditions', 'law enforcement', 'Waste of resources / overkill'
        ]
      },
      desired_values: {
        list: ['Honesty', 'Peace', 'Islamic principles', 'Prosperity', 'law enforcement', 'Healthcare', 'Financial stability',
          'Tolerance and forgiveness', 'Positive', 'Quality of Life']
      },
      total: 188
    },
    east_male_0: {
      question: 'The values chosen by <span style="color: #ffbc00;">males</span> from ages <span style="color: #ffbc00;"> 15 - 25 </span> who live in <span style="color: #ffbc00;"> east </span>',
      personal_values: {
        list: ['Positive', 'Discipline', ' Innovation', 'integrity', 'Workmanship', 'Logic', ' Honesty', 'Financial Stability',
          'live the moment', 'Islamic principles']
      },
      cultural_values: {
        list: ['Honor and morality', 'Confidence', 'Provide housing', 'Agressive behavior', 'Bureaucracy', 'Take care of those in need',
          'Commitment', 'Corruption', 'acceptance of the other', 'Animal Care'
        ]
      },
      desired_values: {
        list: ['Provide housing', 'Agressive behavior', 'Confidence', 'Bureaucracy', 'Commitment', 'Honor and morality',
          'job opportunities', 'Take care of those in need', 'acceptance of the other', 'Corruption']
      },
      total: 110
    },
    west_male_0: {
      question: 'The values chosen by <span style="color: #ffbc00;">males</span> from ages <span style="color: #ffbc00;"> 15 - 25 </span> who live in <span style="color: #ffbc00;"> west </span>',
      personal_values: {
        list: ['Positive', ' Workmanship', ' Honesty', 'The friendship', 'Confidence', 'Certainty and extent of faith', 'Capacity chest',
          'Respect', 'The vineyard', 'Safety']
      },
      cultural_values: {
        list: ['Honesty', 'Confidence', 'Positive', 'Mercy', 'Wisdom', 'Honor and morality', 'Justice and Transparency', 'Patience',
          'Dialogue', 'Jackets on your brother'
        ]
      },
      desired_values: {
        list: ['Provide housing', 'Agressive behavior', 'Confidence', 'Bureaucracy', 'Commitment', 'Honor and morality',
          'job opportunities', 'Take care of those in need', 'acceptance of the other', 'Corruption']
      },
      total: 187
    },
    south_male_0: {
      question: 'The values chosen by <span style="color: #ffbc00;">males</span> from ages <span style="color: #ffbc00;"> 15 - 25 </span> who live in <span style="color: #ffbc00;"> south </span>',
      personal_values: {
        list: ['Patience', 'The friendship', 'The vineyard', 'self confidence',
          'Respect', ' Honesty', 'Moderation', 'Positive', 'The initiative',
          ' Capacity chest']
      },
      cultural_values: {
        list: ['Effective Government', 'Leadership', 'Islamic principles',
          'Military force', 'law enforcement', 'Interest in the next generation',
          'Positive', 'Peace', 'Justice and Transparency', 'integrity']
      },
      desired_values: {
        list: ['Educational opportunities', 'job opportunities', 'Healthcare',
          'Quality of Life', 'Positive', 'Provide housing', 'Family bonding',
          'The fight against poverty', 'Financial stability',
          'Interest in the next generation']
      },
      total: 120
    },
    north_male_0: {
      question: 'The values chosen by <span style="color: #ffbc00;">males</span> from ages <span style="color: #ffbc00;"> 15 - 25 </span> who live in <span style="color: #ffbc00;"> north </span>',
      personal_values: {
        list: ['The friendship', 'The vineyard', 'Honor and morality', 'Equality', 'Patience', 'Experience', 'Respect', 'the health',
          'self confidence', 'Equity']
      },
      cultural_values: {
        list: ['Honesty', 'human rights', 'Positive', 'Openness to the world', 'Justice and Transparency', 'Respect', 'Leadership',
          'Aelovesah', 'Innovation', 'freedom of expression'
        ]
      },
      desired_values: {
        list: ['Honesty', 'Positive', 'Justice and Transparency', 'Tolerance and forgiveness', 'Financial stability', 'human rights',
          'Quality of Life', 'freedom of expression', 'Educational opportunities', 'Leadership']
      },
      total: 65
    },
    center_male_1: {
      question: 'The values chosen by <span style="color: #ffbc00;">males</span> from ages <span style="color: #ffbc00;"> 25 - 35 </span> who live in <span style="color: #ffbc00;"> central region </span>',
      personal_values: {
        list: ['Honor and morality', 'Patience', 'The friendship', ' Capacity chest', 'self confidence', 'The vineyard',
          'Gratitude and thanks', 'Certainty and extent of faith', 'Cooperate', 'Islamic principles']
      },
      cultural_values: {
        list: ['Islamic principles', 'Family bonding', 'Love offense systems / bucking know', 'Traditions', 'Love materialism', 'Honesty',
          'Aelovesah', 'Waste of resources / overkill', 'Blame', 'Military force'
        ]
      },
      desired_values: {
        list: ['Peace', 'Honesty', 'Islamic principles', 'Financial stability', 'Healthcare', 'law enforcement', 'Positive',
          'Tolerance and forgiveness', 'job opportunities', 'Provide housing']
      },
      total: 153
    },
    east_male_1: {
      question: 'The values chosen by <span style="color: #ffbc00;">males</span> from ages <span style="color: #ffbc00;"> 25 - 35 </span> who live in <span style="color: #ffbc00;"> east </span>',
      personal_values: {
        list: ['Professional growth Professional by', 'Quality of Life', 'Financial Stability', 'Positive', 'Logic', 'Risk', 'Innovation',
          'Workmanship', 'Enthusiasm', 'Personal Growth']
      },
      cultural_values: {
        list: ['Honor and morality', 'Confidence', 'Commitment', 'Bureaucracy', 'Agressive behavior', 'Take care of those in need',
          'Corruption', 'Provide housing', 'job opportunities', 'Jackets on your brother'
        ]
      },
      desired_values: {
        list: ['Provide housing', 'Equity', 'Agressive behavior', 'Take care of those in need', 'Commitment', 'Bureaucracy', 'Confidence',
          'Corruption', 'Honor and morality', 'Positive']
      },
      total: 88
    },
    west_male_1: {
      question: 'The values chosen by <span style="color: #ffbc00;">males</span> from ages <span style="color: #ffbc00;"> 25 - 35 </span> who live in <span style="color: #ffbc00;"> west </span>',
      personal_values: {
        list: ['Positive', 'Honesty', 'Honor and morality', 'Confidence', 'The friendship', 'Workmanship', 'Islamic principles',
          'Advise others well', 'the health', 'self confidence']
      },
      cultural_values: {
        list: ['Honesty', 'Honor and morality', 'Positive', 'Confidence', 'Wisdom', 'Awareness of values', 'Mercy',
          'Justice and Transparency', 'Islamic principles', 'law enforcement'
        ]
      },
      desired_values: {
        list: ['Honesty', 'Honor and morality', 'Positive', 'Wisdom', 'Confidence', 'Financial stability', 'Provide housing',
          'Mercy', 'Healthcare', 'Educational opportunities']
      },
      total: 149
    },
    south_male_1: {
      question: 'The values chosen by <span style="color: #ffbc00;">males</span> from ages <span style="color: #ffbc00;"> 25 - 35 </span> who live in <span style="color: #ffbc00;"> south </span>',
      personal_values: {
        list: ['Patience', 'The friendship', 'The vineyard', 'Respect',
          'self confidence', 'Positive', 'the health', 'Safety', 'Wisdom',
          ' Honesty']
      },
      cultural_values: {
        list: ['Military force', 'Islamic principles', 'Effective Government',
          'Family bonding', 'Leadership', 'human rights', 'Innovation',
          'integrity', 'law enforcement', 'Waste of resources / overkill']
      },
      desired_values: {
        list: ['Educational opportunities', 'job opportunities', 'Healthcare',
          'The fight against poverty', 'Interest in the next generation',
          'Positive', 'Quality of Life', 'Provide housing', 'The unemployment',
          'Financial stability']
      },
      total: 94
    },
    north_male_1: {
      question: 'The values chosen by <span style="color: #ffbc00;">males</span> from ages <span style="color: #ffbc00;"> 25 - 35 </span> who live in <span style="color: #ffbc00;"> north </span>',
      personal_values: {
        list: ['The friendship', 'Honor and morality', 'Experience', 'Tolerance and forgiveness', 'The vineyard', 'self confidence',
          'Patience', 'Respect', 'Equality', 'Justice']
      },
      cultural_values: {
        list: ['Honesty', 'human rights', 'Positive', 'Leadership', 'freedom of expression', 'law enforcement', 'Justice and Transparency',
          'Innovation', 'Cooperat', 'The unemployment'
        ]
      },
      desired_values: {
        list: ['Honesty', 'Justice and Transparency', 'Respect', 'Financial stability', 'human rights', 'Positive', 'Cooperat', 'integrity',
          'Openness to the world', 'Legal Rights']
      },
      total: 51
    },
    center_male_2: {
      question: 'The values chosen by <span style="color: #ffbc00;">males</span> from ages <span style="color: #ffbc00;"> 35+ </span> who live in <span style="color: #ffbc00;"> central region </span>',
      personal_values: {
        list: ['Patience', 'Honor and morality', 'The vineyard', 'Islamic principles', 'Gratitude and thanks', 'Cooperate', 'Mercy',
          'Certainty and extent of faith', 'Positive', 'Careful reputation']
      },
      cultural_values: {
        list: ['Islamic principles', 'Family bonding', 'Traditions', 'Care of the elderly', 'law enforcement', 'Peace',
          'Educational opportunities', 'Effective Government', 'Take care of those in need', 'Love materialism'
        ]
      },
      desired_values: {
        list: ['Peace', 'Islamic principles', 'Honesty', 'Healthcare', 'Family bonding', 'law enforcement', 'Provide housing',
          'Tolerance and forgiveness', 'Financial stability', 'Justice and Transparency']
      },
      total: 223
    },
    east_male_2: {
      question: 'The values chosen by <span style="color: #ffbc00;">males</span> from ages <span style="color: #ffbc00;"> 35+ </span> who live in <span style="color: #ffbc00;"> east </span>',
      personal_values: {
        list: ['Positive', 'Risk', ' Honesty', 'integrity', 'Financial Stability', 'Innovation', 'Professional growth Professional by',
          'Enthusiasm', 'live the moment', 'Achievement']
      },
      cultural_values: {
        list: ['Confidence', 'Bureaucracy', 'Agressive behavior', 'Commitment', 'Honor and morality', 'Provide housing', 'Wisdom',
          'Corruption', 'Take care of those in need', 'acceptance of the other'
        ]
      },
      desired_values: {
        list: ['Provide housing', 'Commitment', 'Confidence', 'acceptance of the other', 'Bureaucracy', 'Take care of those in need',
          'Agressive behavior', 'Honor and morality', 'Mercy', 'Government services, reliable']
      },
      total: 123
    },
    west_male_2: {
      question: 'The values chosen by <span style="color: #ffbc00;">males</span> from ages <span style="color: #ffbc00;"> 35+ </span> who live in <span style="color: #ffbc00;"> west </span>',
      personal_values: {
        list: [' Workmanship', 'Positive', ' Honesty', 'Discipline', 'Islamic principles', 'Confidence', 'Advise others well',
          'Certainty and extent of faith', 'Adapt to variables', 'Determination and perseverance']
      },
      cultural_values: {
        list: ['Honesty', 'Confidence', 'Awareness of values', 'Patience', 'Positive', 'Honor and morality', 'Wisdom', 'Dialogue',
          'creativity', 'Justice and Transparency'
        ]
      },
      desired_values: {
        list: ['Honesty', 'Educational opportunities', 'Complacency', 'Honor and morality', 'Healthcare', 'Financial stability',
          'Confidence', 'Positive', 'personal freedom', 'Wisdom']
      },
      total: 257
    },
    south_male_2: {
      question: 'The values chosen by <span style="color: #ffbc00;">males</span> from ages <span style="color: #ffbc00;"> 35+ </span> who live in <span style="color: #ffbc00;"> south </span>',
      personal_values: {
        list: ['The vineyard', 'Patience', 'Respect', 'Humility', 'The friendship',
          'self confidence', 'Affiliation', ' Honesty', 'Justice', 'Moderation']
      },
      cultural_values: {
        list: ['Family bonding', 'Effective Government', 'Military force',
          'Justice and Transparency', 'Leadership',
          'Interest in the next generation', 'Waste of resources / overkill',
          'law enforcement', 'integrity', 'Islamic principles']
      },
      desired_values: {
        list: ['Educational opportunities', 'Healthcare', 'job opportunities',
          'Family bonding', 'Quality of Life', 'The fight against poverty',
          'Provide housing', 'Financial stability',
          'Interest in the next generation', 'Positive']
      },
      total: 149
    },
    north_male_2: {
      question: 'The values chosen by <span style="color: #ffbc00;">males</span> from ages <span style="color: #ffbc00;"> 35+ </span> who live in <span style="color: #ffbc00;"> north </span>',
      personal_values: {
        list: ['The friendship', 'Experience', 'The vineyard', 'Honor and morality', 'Equality', 'Patience', 'Tolerance and forgiveness',
          'the health', 'Safety', 'Certainty and extent of faith']
      },
      cultural_values: {
        list: ['Honesty', 'Positive', 'freedom of expression', 'Leadership', 'law enforcement', 'Effective Government',
          'Islamic principles', 'Innovation', 'Tolerance and forgiveness', 'human rights'
        ]
      },
      desired_values: {
        list: ['Honesty', 'Tolerance and forgiveness', 'Positive', 'Respect', 'Complacency', 'Justice and Transparency', 'Innovation',
          'human rights', 'Financial stability', 'Islamic principles']
      },
      total: 71
    },
    center_female_0: {
      question: 'The values chosen by <span style="color: #ffbc00;">females</span> from ages <span style="color: #ffbc00;"> 15 - 25 </span> who live in <span style="color: #ffbc00;"> central region </span>',
      personal_values: {
        list: ['The friendship', 'Patience', 'Respect', 'self confidence', 'Honesty', 'The vineyard', 'Mercy', 'Positive', 'the health',
          'Self-Take responsibility']
      },
      cultural_values: {
        list: ['The unemployment', 'Honesty', 'Discrimination by sex', 'Positive', 'Openness to the world', 'human rights',
          'Ridicule others / Bullying', 'Family bonding', 'Tribal fanaticism', 'Leadership'
        ]
      },
      desired_values: {
        list: ['Educational opportunities', 'The fight against poverty', 'Justice and Transparency', 'job opportunities',
          'Financial stability', 'freedom of expression', 'Interest in the next generation', 'human rights', 'Honesty', 'Family bonding']
      },
      total: 175
    },
    east_female_0: {
      question: 'The values chosen by <span style="color: #ffbc00;">females</span> from ages <span style="color: #ffbc00;"> 15 - 25 </span> who live in <span style="color: #ffbc00;"> east </span>',
      personal_values: {
        list: ['Honor and morality', 'Equality', 'The friendship', 'Experience', 'self confidence', 'Patience', 'The vineyard',
          'Tolerance and forgiveness', 'Positive', 'the health']
      },
      cultural_values: {
        list: ['freedom of expression', 'Honesty', 'Positive', 'Tolerance and forgiveness', 'Fear of the future', 'law enforcement',
          'parasitism', 'Military force', 'Openness to the world', 'Ridicule others / Bullying'
        ]
      },
      desired_values: {
        list: ['Financial stability', 'Positive', 'Honesty', 'Tolerance and forgiveness', 'personal freedom', 'freedom of expression',
          'Educational opportunities', 'Openness to the world', 'Family bonding', 'Complacency']
      },
      total: 96
    },
    west_female_0: {
      question: 'The values chosen by <span style="color: #ffbc00;">females</span> from ages <span style="color: #ffbc00;"> 15 - 25 </span> who live in <span style="color: #ffbc00;"> west </span>',
      personal_values: {
        list: [' Workmanship', 'Positive', 'Courage', ' Honesty', 'Humility', 'integrity',
          'Help learning / care and provide advice and guidance', 'Quality of Life', 'creativity', 'Determination and perseverance']
      },
      cultural_values: {
        list: ['creativity', 'Jackets on your brother', 'Dialogue', 'Peace', 'Take care of those in need', 'job opportunities',
          'Honor and morality', 'personal freedom', 'Government services, reliable', 'Equity'
        ]
      },
      desired_values: {
        list: ['personal freedom', 'Quality of Life', 'Peace', 'Financial stability', 'Educational opportunities', 'Healthcare',
          'Justice and Transparency', 'Complacency', 'Honesty', 'Opportunities for entrepreneurship']
      },
      total: 183
    },
    south_female_0: {
      question: 'The values chosen by <span style="color: #ffbc00;">females</span> from ages <span style="color: #ffbc00;"> 15 - 25 </span> who live in <span style="color: #ffbc00;"> south </span>',
      personal_values: {
        list: ['Respect', 'The friendship', 'Justice', 'Safety', 'self confidence',
          'The initiative', 'Pride', 'Self-Take responsibility',
          'Adapt to variables', 'Patience']
      },
      cultural_values: {
        list: ['Blurred vision', 'creativity', 'Justice and Transparency',
          'human rights', 'Opportunities for entrepreneurship', 'Envy',
          'Altruism', 'Blame', 'Dialogue', 'Positive']
      },
      desired_values: {
        list: ['Educational opportunities', 'Healthcare', 'Quality of Life',
          'Positive', 'Cooperat', 'Complacency', 'Justice and Transparency',
          'human rights', 'The fight against poverty', 'Respect']
      },
      total: 119
    },
    north_female_0: {
      question: 'The values chosen by <span style="color: #ffbc00;">females</span> from ages <span style="color: #ffbc00;"> 15 - 25 </span> who live in <span style="color: #ffbc00;"> north </span>',
      personal_values: {
        list: ['The friendship', 'self confidence', 'Patience', 'The vineyard', 'Honor and morality', 'Safety', 'Tolerance and forgiveness',
          'personal freedom', 'Justice', 'Positive']
      },
      cultural_values: {
        list: ['Honesty', 'Positive', 'Justice and Transparency', 'Islamic principles', 'Respect', 'Traditions',
          'Tolerance and forgiveness', 'law enforcement', 'Family bonding', 'Poverty'
        ]
      },
      desired_values: {
        list: ['Honesty', 'Positive', 'Nature Conservation', 'Respect', 'Take care of those in need', 'Complacency', 'Financial stability',
          'Respect for the time', 'Islamic principles', 'Tolerance and forgiveness']
      },
      total: 64
    },
    center_female_1: {
      question: 'The values chosen by <span style="color: #ffbc00;">females</span> from ages <span style="color: #ffbc00;"> 25 - 35 </span> who live in <span style="color: #ffbc00;"> central region </span>',
      personal_values: {
        list: ['The friendship', 'Positive', 'Honor and morality', 'Patience', 'self confidence', 'Gratitude and thanks',
          'Tolerance and forgiveness', 'self-reliance', 'The vineyard', 'Certainty and extent of faith']
      },
      cultural_values: {
        list: ['Islamic principles', 'law enforcement', 'Traditions', 'Honesty', 'Peace', 'Interest in the world', 'Positive',
          'Openness to the world', 'Love materialism', 'Effective Government'
        ]
      },
      desired_values: {
        list: ['job opportunities', 'Honesty', 'The fight against poverty', 'Financial stability', 'Family bonding',
          'Educational opportunities', 'Islamic principles', 'human rights', 'Positive', 'Healthcare']
      },
      total: 152
    },
    east_female_1: {
      question: 'The values chosen by <span style="color: #ffbc00;">females</span> from ages <span style="color: #ffbc00;"> 25 - 35 </span> who live in <span style="color: #ffbc00;"> east </span>',
      personal_values: {
        list: ['Honor and morality', 'Equality', 'Patience', 'The friendship', 'Experience', 'Safety', 'The vineyard', 'self confidence',
          'Self-realization', 'the health']
      },
      cultural_values: {
        list: ['Honesty', 'Positive', 'Tolerance and forgiveness', 'freedom of expression', 'Fear of the future', 'Financial stability',
          'Diversity', 'Islamic principles', 'Educational opportunities', 'human rights'
        ]
      },
      desired_values: {
        list: ['Financial stability', 'Honesty', 'Tolerance and forgiveness', 'personal freedom', 'Quality of Life', 'Complacency',
          'freedom of expression', 'Islamic principles', 'Positive', 'law enforcement']
      },
      total: 90
    },
    west_female_1: {
      question: 'The values chosen by <span style="color: #ffbc00;">females</span> from ages <span style="color: #ffbc00;"> 25 - 35 </span> who live in <span style="color: #ffbc00;"> west </span>',
      personal_values: {
        list: ['Positive', ' Workmanship', 'Help learning / care and provide advice and guidance', ' Honesty',
          'Courage', 'Cooperate', 'Humility', 'Complacency', 'To be the best', 'Determination and perseverance']
      },
      cultural_values: {
        list: ['personal freedom', 'creativity', 'Dialogue', 'Jackets on your brother', 'Educational opportunities', 'Diversity',
          'Equity', 'Positive', 'Quality of Life', 'Honor and morality'
        ]
      },
      desired_values: {
        list: ['Peace', 'personal freedom', 'Quality of Life', 'Financial stability', 'Honesty', 'Educational opportunities',
          'Family bonding', 'Government services, reliable', 'Healthcare', 'job opportunities']
      },
      total: 159
    },
    south_female_1: {
      question: 'The values chosen by <span style="color: #ffbc00;">females</span> from ages <span style="color: #ffbc00;"> 25 - 35 </span> who live in <span style="color: #ffbc00;"> south </span>',
      personal_values: {
        list: ['self confidence', 'Adapt to variables', 'Patience', 'Moderation',
          'The friendship', 'Safety', 'Volunteering', 'The vineyard', 'Justice',
          'the health']
      },
      cultural_values: {
        list: ['Educational opportunities', 'Cooperat', 'Healthcare', 'Animal Care',
          'Interest in the next generation', 'freedom of expression',
          'Family bonding', 'Openness to the world', 'human rights', 'Positive']
      },
      desired_values: {
        list: ['Interest in the next generation', 'Educational opportunities',
          'The fight against poverty', 'Family bonding', 'Prosperity', 'Positive',
          'Quality of Life', 'Animal Care', 'Cooperat', 'Appreciation of beauty']
      },
      total: 106
    },
    north_female_1: {
      question: 'The values chosen by <span style="color: #ffbc00;">females</span> from ages <span style="color: #ffbc00;"> 25 - 35 </span> who live in <span style="color: #ffbc00;"> north </span>',
      personal_values: {
        list: ['Patience', 'The vineyard', 'The friendship', 'Honor and morality', 'self confidence', 'Positive', 'The initiative',
          'Equality', 'Capacity chest', 'Listening']
      },
      cultural_values: {
        list: ['Honesty', 'The unemployment', 'freedom of expression', 'Leadership', 'law enforcement', 'The scandal',
          'Discrimination by sex', 'Tolerance and forgiveness', 'illiteracy', 'Ridicule others / Bullying'
        ]
      },
      desired_values: {
        list: ['Honesty', 'Tolerance and forgiveness', 'Islamic principles', 'Respect', 'Quality of Life', 'Complacency',
          'Financial stability', 'Determination and perseverance', 'Educational opportunities', 'Effective Government']
      },
      total: 53
    },
    center_female_2: {
      question: 'The values chosen by <span style="color: #ffbc00;">females</span> from ages <span style="color: #ffbc00;"> 35+ </span> who live in <span style="color: #ffbc00;"> central region </span>',
      personal_values: {
        list: ['Honor and morality', 'Positive', 'Patience', 'The friendship', 'Careful reputation', ' Capacity chest', 'Honesty',
          'Certainty and extent of faith', 'The vineyard', 'Humility']
      },
      cultural_values: {
        list: ['Islamic principles', 'law enforcement', 'Traditions', 'human rights', 'Care of the elderly', 'Love materialism',
          'Educational opportunities', 'Family bonding', 'Peace', 'Aelovesah'
        ]
      },
      desired_values: {
        list: ['Islamic principles', 'Honesty', 'job opportunities', 'Family bonding', 'Interest in the next generation', 'Peace',
          'Positive', 'The fight against poverty', 'Provide housing', 'Healthcare']
      },
      total: 208
    },
    east_female_2: {
      question: 'The values chosen by <span style="color: #ffbc00;">females</span> from ages <span style="color: #ffbc00;"> 35+ </span> who live in <span style="color: #ffbc00;"> east </span>',
      personal_values: {
        list: ['Honor and morality', 'Equality', 'Patience', 'Experience', 'Tolerance and forgiveness', 'The friendship', 'The vineyard',
          'Safety', 'Certainty and extent of faith', 'Adapt to variables']
      },
      cultural_values: {
        list: ['Ridicule others / Bullying', 'job opportunities', 'creativity', 'Fear of the future', 'Educational opportunities',
          'freedom of expression', 'Honesty', 'Jackets on your brother', 'Corruption', 'Take care of those in need'
        ]
      },
      desired_values: {
        list: ['Honesty', 'personal freedom', 'job opportunities', 'Quality of Life', 'Educational opportunities', 'Complacency',
          'Financial stability', 'Family bonding', 'Peace', 'Positive']
      },
      total: 109
    },
    west_female_2: {
      question: 'The values chosen by <span style="color: #ffbc00;">females</span> from ages <span style="color: #ffbc00;"> 35+ </span> who live in <span style="color: #ffbc00;"> west </span>',
      personal_values: {
        list: ['Positive', ' Workmanship', 'Cooperate', 'Humility', 'Help learning / care and provide advice and guidance', 'Honesty',
          'Capacity chest', 'Courage', 'To be the best', 'integrity']
      },
      cultural_values: {
        list: ['personal freedom', 'Peace', 'creativity', 'Dialogue', 'Honesty', 'Jackets on your brother', 'Justice and Transparency',
          'Diversity', 'Educational opportunities', 'Positive'
        ]
      },
      desired_values: {
        list: ['Quality of Life', 'personal freedom', 'Educational opportunities', 'Peace', 'Financial stability', 'Honesty', 'Healthcare',
          'job opportunities', 'The fight against poverty', 'Government services, reliable']
      },
      total: 241
    },
    south_female_2: {
      question: 'The values chosen by <span style="color: #ffbc00;">females</span> from ages <span style="color: #ffbc00;"> 35+ </span> who live in <span style="color: #ffbc00;"> south </span>',
      personal_values: {
        list: ['Safety', 'Patience', 'the health', 'The vineyard', 'The friendship',
          'Experience', 'Honor and morality', 'Affiliation', 'Keeping Promise',
          'Equity']
      },
      cultural_values: {
        list: ['Family bonding', 'Volunteering', 'Blurred vision', 'Positive',
          'Interest in the next generation', 'Justice and Transparency',
          'Certainty and extent of faith', 'The unemployment', 'Cooperat',
          'Educational opportunities']
      },
      desired_values: {
        list: ['Educational opportunities', 'Interest in the next generation',
          'Quality of Life', 'Healthcare', 'Positive', 'Cooperat',
          'Justice and Transparency', 'Animal Care', 'The fight against poverty',
          'Family bonding']
      },
      total: 149
    },
    north_female_2: {
      question: 'The values chosen by <span style="color: #ffbc00;">females</span> from ages <span style="color: #ffbc00;"> 35+ </span> who live in <span style="color: #ffbc00;"> north </span>',
      personal_values: {
        list: ['The vineyard', 'Patience', 'The friendship', 'Honor and morality', 'Safety', 'self confidence', 'Tolerance and forgiveness',
          'the health', 'Experience', 'Islamic principles']
      },
      cultural_values: {
        list: ['Honesty', 'human rights', 'freedom of expression', 'The unemployment', 'Legal Rights', 'Jackets on your brother',
          'law enforcement', 'Justice and Transparency', 'Family bonding', 'Quality of Life'
        ]
      },
      desired_values: {
        list: ['Honesty', 'Islamic principles', 'Tolerance and forgiveness', 'Positive', 'Respect', 'Quality of Life',
          'Nature Conservation', 'Family bonding', 'Interest in the next generation', 'Effective Government']
      },
      total: 69
    },
    all_male: {
      question: 'The values chosen by <span style="color: #ffbc00;">males</span> from <span style=\'color: #ffbc00;\'>all</span> ages who live in <span style=\'color: #ffbc00;\'>Saudi Arabia</span>',
      personal_values: {
        list: ['Positive', 'Patience', 'The friendship', 'Honor and morality',
          ' Honesty', 'The vineyard', 'Islamic principles',
          'Certainty and extent of faith', 'Respect', 'self confidence']
      },
      cultural_values: {
        list: ['Islamic principles', 'Honesty', 'Family bonding', 'Honor and morality',
          'Positive', 'Traditions', 'Effective Government',
          'Take care of those in need', 'law enforcement', 'Confidence']
      },
      desired_values: {
        list: ['Honesty', 'Healthcare', 'Educational opportunities', 'Positive',
          'job opportunities', 'Peace', 'Financial stability', 'Provide housing',
          'Islamic principles', 'Honor and morality']
      },
      total: 2028
    },
    all_female: {
      question: 'The values chosen by <span style="color: #ffbc00;">females</span> from <span style=\'color: #ffbc00;\'>all</span> ages who live in <span style=\'color: #ffbc00;\'>Saudi Arabia</span>',
      personal_values: {
        list: ['Positive', 'Honor and morality', 'The friendship', 'Patience',
          'The vineyard', 'self confidence', 'Tolerance and forgiveness',
          ' Workmanship', 'Adapt to variables', 'Equality']
      },
      cultural_values: {
        list: ['Honesty', 'Positive', 'Islamic principles',
          'Educational opportunities', 'law enforcement', 'Family bonding',
          'Justice and Transparency', 'Interest in the next generation',
          'creativity', 'human rights']
      },
      desired_values: {
        list: ['Honesty', 'Educational opportunities', 'Quality of Life',
          'Financial stability', 'Positive', 'personal freedom', 'Healthcare',
          'job opportunities', 'Family bonding',
          'Interest in the next generation']
      },
      total: 1973
    },
    all_0: {
      question: "The values chosen by <span style='color: #ffbc00;'>all Saudi's</span> from ages <span style='color: #ffbc00;'>15 - 25</span>  who live in <span style='color: #ffbc00;'>Saudi Arabia</span>",
      personal_values: {
        list: ['Positive', 'The friendship', 'Patience', 'The vineyard', 'Respect',
          'Honor and morality', 'self confidence', ' Workmanship', ' Honesty',
          'Safety']
      },
      cultural_values: {
        list: ['Honesty', 'Positive', 'Family bonding', 'Islamic principles',
          'Justice and Transparency', 'law enforcement',
          'Interest in the next generation', 'Take care of those in need',
          'Traditions', 'Openness to the world']
      },
      desired_values: {
        list: ['Honesty', 'Educational opportunities', 'Positive',
          'Financial stability', 'Quality of Life', 'Healthcare',
          'job opportunities', 'Justice and Transparency', 'Peace',
          'Complacency']
      },
      total: 1307
    },
    all_1: {
      question: "The values chosen by <span style='color: #ffbc00;'>all Saudi's</span> from ages <span style='color: #ffbc00;'>25 - 35</span>  who live in <span style='color: #ffbc00;'>Saudi Arabia</span>",
      personal_values: {
        list: ['Positive', 'Honor and morality', 'The friendship', 'Patience',
          'self confidence', 'The vineyard', ' Honesty', 'Respect', 'the health',
          'Humility']
      },
      cultural_values: {
        list: ['Honesty', 'Islamic principles', 'Positive', 'Family bonding',
          'Educational opportunities', 'Traditions', 'law enforcement',
          'Honor and morality', 'Military force', 'Leadership']
      },
      desired_values: {
        list: ['Honesty', 'Financial stability', 'Educational opportunities',
          'Positive', 'job opportunities', 'Healthcare', 'Quality of Life',
          'Peace', 'Islamic principles', 'Family bonding']
      },
      total: 1095
    },
    all_2: {
      question: "The values chosen by <span style='color: #ffbc00;'>all Saudi's</span> from ages <span style='color: #ffbc00;'>35+</span>  who live in <span style='color: #ffbc00;'>Saudi Arabia</span>",
      personal_values: {
        list: ['Positive', 'The friendship', 'Patience', 'The vineyard', 'Respect',
          'Honor and morality', 'self confidence', ' Workmanship', ' Honesty',
          'Safety']
      },
      cultural_values: {
        list: ['Islamic principles', 'Honesty', 'Family bonding', 'Traditions',
          'law enforcement', 'Educational opportunities', 'Positive',
          'Effective Government', 'Peace', 'Take care of those in need']
      },
      desired_values: {
        list: ['Honesty', 'Educational opportunities', 'Healthcare', 'Peace',
          'Financial stability', 'job opportunities', 'Quality of Life',
          'Islamic principles', 'Positive', 'Family bonding']
      },
      total: 1599
    },
    all: {
      question: "The values chosen by <span style='color: #ffbc00;'>all Saudi's</span> from <span style='color: #ffbc00;'>all</span> ages who live in <span style='color: #ffbc00;'>Saudi Arabia</span>",
      personal_values: {
        list: ['Positive', 'Honor and morality', 'The friendship', 'Patience',
          'The vineyard', 'self confidence', ' Honesty', ' Workmanship',
          'Respect', 'Humility'],
        list_details: [
          {
            list_detail: 'An optimistic outlook, acting modestly and with high moral principles'
          },
          {
            list_detail: 'Appreciation for their closest connections, showing consideration and kindness to others'
          },
          {
            list_detail: 'Inner strength and composure, demonstrating accomplishment in their endeavours'
          }
        ]
      },
      cultural_values: {
        list: ['The friendship', 'Islamic principles', 'Positive', 'Family bonding',
          'Traditions', 'Educational opportunities', 'law enforcement',
          'Cooperate', 'Interest in the next generation', 'Peace'],
        list_details: [
          {
            list_detail: 'Actions guided by religious conventions and established practices, and where people are true to themselves'
          },
          {
            list_detail: 'A can-do outlook, where people bond together and are openly just with one another'
          },
          {
            list_detail: 'Strength in national administrative processes and the upholding of rules and regulations'
          },
          {
            list_detail: 'Harmony in the nation, with teaching accessible to all'
          }
        ]
      },
      desired_values: {
        list: ['Educational opportunities', 'Financial stability', 'Positive',
          'Healthcare', 'job opportunities', 'Quality of Life', 'Peace',
          'Islamic principles', 'Family bonding', 'Justice and Transparency'],
        list_details: [
          {
            list_detail: 'People want greater access to learning and jobs, and better medical facilities'
          },
          {
            list_detail: 'They want to ensure economic security and increase their sense of happiness'
          },
          {
            list_detail: 'It is important to them to maintain a sense of tranquility in the nation and continue to uphold ' +
              'religious teachings'
          },
          {
            list_detail: 'They wish to enhance their sense of optimism and encourage openness and fairness in society'
          }
        ]
      },
      total: 4001
    }
  };

  dataAgainstEvent: any;
  key = '';

  showInfoContainer = false;

  constructor() {
  }

  ngOnInit() {
    this.dataAgainstEvent = this.clickedVectorData.all;

    setTimeout(() => {
      this.showInfoContainer = true;
    }, 500);

  }

  eventClicked(values: any) {
    this.key = values;
    console.log(this.key)

    switch (this.key) {
      case 'all_female_0':
        this.key = 'female_0';
        break;
      case 'all_female_1':
        this.key = 'female_1';
        break;
      case 'all_female_2':
        this.key = 'female_2';
        break;
      case 'all_male_0':
        this.key = 'male_0';
        break;
      case 'all_male_1':
        this.key = 'male_1';
        break;
      case 'all_male_2':
        this.key = 'male_2';
        break;
      default:
    }

    this.dataAgainstEvent = this.clickedVectorData[this.key];
    if (this.dataAgainstEvent.total === undefined) {
      this.dataAgainstEvent.total = 4001
    }
  }

}
