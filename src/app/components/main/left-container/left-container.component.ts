import {
  Component,
  EventEmitter,
  Input,
  OnChanges,
  OnInit,
  Output,
  SimpleChanges,
  ViewEncapsulation
} from '@angular/core';
import {animate, keyframes, query, stagger, style, transition, trigger} from '@angular/animations';
import {DomSanitizer} from "@angular/platform-browser";

@Component({
  selector: 'app-left-container',
  templateUrl: './left-container.component.html',
  styleUrls: ['./left-container.component.css'],
  animations: [
    // Trigger animation cards array
    trigger('listAnimation', [
      // Transition from any state to any state
      transition('* => *', [
        // Initially the all cards are not visible
        query(':enter', style({opacity: 0}), {optional: true}),

        // Each card will appear sequentially with the delay of 300ms
        query(':enter', stagger('80ms', [
          animate('.5s ease-in', keyframes([
            style({opacity: 0, transform: 'translateY(-50%)', offset: 0}),
            style({opacity: 1, transform: 'translateY(0)', offset: 1}),
          ]))]), {optional: true}),
      ]),
    ]),
  ]
})
export class LeftContainerComponent implements OnInit, OnChanges {
  // gender flags
  isMale = false;
  isFemale = false;

  // map flags
  map: any;
  mapRegion: any;

  regionFlags = {
    east: true,
    west: true,
    center: true,
    south: true,
    north: true,
  };

  regionArray = ['east', 'west', 'center', 'south', 'north'];

  east = true;
  west = true;
  center = true;
  south = true;
  north = true;

  // age flags
  ageLimit = [false, false, false];


  maleSvg = '../../../../assets/images/male.svg';
  femaleSvg = '../../../../assets/images/female.svg';
  maleFilledSvg = '../../../../assets/images/male-filled.svg';
  femaleFilledSvg = '../../../../assets/images/female-filled.svg';

  selectedArea: string;
  selectedValue: any;


  @Input() data: any;

  @Input() key: string | number;

  @Output()
  eventClicked = new EventEmitter();


  // keys object
  object: any = {
    region: '',
    gender: '',
    age: ''
  };


  constructor(private sanitizer: DomSanitizer) {
  }

  ngOnInit() {
    this.object.region = 'all';
    this.map = document.getElementById('map');
    const allAreas = this.map.querySelectorAll('g');
    this.map.addEventListener('click', (e: any) => {
      this.mapRegion = e.target.parentNode;
      if (e.target.nodeName === 'path') {

        if ((this.mapRegion.id === 'center' && !this.regionFlags.center)
          || (this.mapRegion.id === 'east' && !this.regionFlags.east)
          || (this.mapRegion.id === 'west' && !this.regionFlags.west)
          || (this.mapRegion.id === 'north' && !this.regionFlags.north)
          || (this.mapRegion.id === 'south' && !this.regionFlags.south)
          || (this.object.region === 'all' && this.mapRegion.id === 'center' && this.regionFlags.center)
          || (this.object.region === 'all' && this.mapRegion.id === 'south' && this.regionFlags.south)
          || (this.object.region === 'all' && this.mapRegion.id === 'east' && this.regionFlags.east)
          || (this.object.region === 'all' && this.mapRegion.id === 'west' && this.regionFlags.west)
          || (this.object.region === 'all' && this.mapRegion.id === 'north' && this.north)) {

          this.regionArray.forEach((val) => {
            this.regionFlags[val] = false;
          });
          setTimeout(() => {
            this.regionFlags[this.mapRegion.id] = true;
          }, 0);
          this.object.region = this.mapRegion.id;

        } else if ((this.mapRegion.id === 'center' && this.regionFlags.center)
          || (this.mapRegion.id === 'east' && this.regionFlags.east)
          || (this.mapRegion.id === 'west' && this.regionFlags.west)
          || (this.mapRegion.id === 'north' && this.regionFlags.north)
          || (this.mapRegion.id === 'south' && this.regionFlags.south)) {


          if (this.object.gender !== '' && this.object.age !== '') {
            this.regionArray.forEach((val) => {
              this.regionFlags[val] = false;
              this.object.region = '';
            });
          } else if (this.object.gender || this.object.age) {
            this.regionArray.forEach((val) => {
              this.regionFlags[val] = false;
              this.object.region = '';
            });
          }


        }

        this.selectedArea = this.mapRegion.id;
        this.selectedValue = {type: 'region', value: this.selectedArea};

        this.eventClicked.emit(this.createKey());
      }
    });

  }

  genderClicked(type) {
    this.selectedValue = {type: 'gender', value: type};

    if (type === 'male' && !this.isMale) {
      this.object.gender = type;
      this.isMale = true;
      this.isFemale = false;
    } else if (type === 'female' && !this.isFemale) {
      this.object.gender = type;
      this.isMale = false;
      this.isFemale = true;
    } else if ((type === 'female' && this.isFemale) || (type === 'male' && this.isMale)) {

      this.object.gender = '';
      this.isMale = false;
      this.isFemale = false;

    }

    this.eventClicked.emit(this.createKey());
  }

  ageFilter(type) {
    this.selectedValue = {type: 'age', value: type};

    if (this.ageLimit[type] === false) {
      for (let i = 0; i < this.ageLimit.length; i++) {
        this.ageLimit[i] = false;
      }
      this.ageLimit[type] = true;
      this.object.age = type;
    } else {
      this.object.age = '';
      for (let i = 0; i < this.ageLimit.length; i++) {
        this.ageLimit[i] = false;
      }
    }
    this.eventClicked.emit(this.createKey());
    // console.log(`${this.object.region}${this.object.gender !== '' ? `_${this.object.gender}` : ''}${this.object.age !== '' ? `_${this.object.age}` : ''}`, 'in age')
  }

  transformYourHtml(htmlTextWithStyle) {
    return this.sanitizer.bypassSecurityTrustHtml(htmlTextWithStyle);
  }

  createKey() {
    const keyString = Array.from(Object.values(this.object).join('_').replace('__', '_'));
    if (keyString[0] === '_') {
      keyString.splice(0, 1);
    }
    if (keyString[keyString.length - 1] === '_') {
      keyString.splice(keyString.length - 1, 1);
    }
    return keyString.join('');
  }

  reset() {
    this.regionFlags = {
      east: true,
      west: true,
      center: true,
      south: true,
      north: true,
    };
    this.isFemale = false;
    this.isMale = false;
    for (let i = 0; i < this.ageLimit.length; i++) {
      this.ageLimit[i] = false;
    }
    this.object = {
      region: '',
      gender: '',
      age: ''
    };
    this.eventClicked.emit(this.object.region = 'all');
  }

  ngOnChanges(changes: SimpleChanges): void {
    this.data = changes.data.currentValue;
    this.key = changes.key.currentValue;
  }
}
